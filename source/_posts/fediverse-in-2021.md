---
layout: "post"
title: "Fediverse in 2021"
date: 2022-01-05
updated: 2022-01-05
tags:
    - fediverse
preview: "Buckle your seatbelts and lets travel back in time. Let us watch the progress of glorious Fediverse in the past year"
url: "/en/post/fediverse-in-2021"
lang: en
banner: "banner.jpg"
authors: [{"name": "@lostinlight", "url": "https://mastodon.xyz/@lightone", "network": "mastodon"}]
---

<span class="u-goldenBg">**Happy 2022, fedizens!**</span>

At the end of each year we traditionally compile a digest of what's happened in Fediverse (beginning with [2019](https://fediverse.party/en/post/fediverse-in-2019) and [2020](https://fediverse.party/en/post/fediverse-in-2020)).

So, buckle your seatbelts and lets travel back in time. Let us watch the progress of glorious Fediverse in the past year.

![Fedi spaceship exploring decentralized worlds](/en/post/fediverse-in-2021/fedi-spaceship.jpg)
<small class="u-block u-center"><span class="u-emphasize">Credits:</span> image from Pixabay</small>

### Fediverse in numbers

In 2021 Fediverse expanded from 5.027 to 7.744 known servers. That's <span class="u-emphasize">about +2.700 nodes</span>! An impressive growth, compared to previous couple of years that showed a stable trend of 900-1.000 new servers a year.

In June *fediverse.party* started counting Plume, Lemmy and Mobilizon in yearly statistics. They account for only 222 servers though, so the rest of the statistics spike definitely indicates that Fediverse is growing! By the way, we [launched](https://nodes.fediverse.party) our own Fediverse crawler in November, to make sure that these numbers are as accurate as possible.

![Pie chart showing most popular TLDs in Fediverse](/en/post/fediverse-in-2021/TLD-piechart.png)
<small class="u-block u-center"><span class="u-emphasize">Credits:</span> pie chart by Minoru</small>

The ammount of registered accounts went back up to 2019 levels and by the end of 2021 slightly surpassed that benchmark, reaching ~4.500.000.

We, fedizens, know that <span class="u-emphasize">numbers aren't the most important thing in the world</span>. But grown-ups like numbers.

![Illustration of Little Prince and the Fox sitting on a planet](/en/post/fediverse-in-2021/little-prince-in-fedi.png)
<small class="u-block u-center"><span class="u-emphasize">Credits:</span> image by Devran Topallar, Pixabay license</small>

So let's mention that, despite smaller userbase, Fediverse is showing its true potential for free open source projects: those developers who officially join our corner of the Internet [enjoy higher follower numbers](https://mastodon.online/@FediFollows/106647703394010040) than on centralized social networks. \O/

Seven networks growing most rapidly in online servers in 2021 were:

• Misskey 🎉 ~ quadrupled server numbers (+220)

• PeerTube 🎉 ~ tripled server numbers (+760)

• Funkwhale 🎉 ~ tripled server numbers (+83)

• Pleroma 🎉 ~ doubled server numbers (+483)

• WriteFreely 🎉 ~ doubled server numbers (+180)

• Pixelfed 🎉 ~ doubled server numbers (+106)

• Mastodon 🎉 ~ +28% (+792)

*Note: these numbers are an approximation based on statistics voluntarily provided by server administrators. Servers go offline / online every hour, every day, so the numbers reflect only part of Fediverse at the time this article was published.*

### Fediverse in projects

At least <span class="u-emphasize">13 new projects</span> joined Fediverse in 2021!

🌟 [GoToSocial](https://github.com/gotosocial/gotosocial) – social network server written in Golang

🌟 [Owncast](https://github.com/owncast/owncast) –  self-hosted live streaming platform

🌟 [Castopod](https://code.podlibre.org/podlibre/castopod) – open-source platform made for podcasters who want to engage and interact with their audience

🌟 [Inventaire](https://github.com/inventaire/inventaire) – platform for cataloguing and sharing physical books from personal collections and independent libraries

🌟 [GoBlog](https://git.jlel.se/jlelse/GoBlog) – simple blogging system written in Go

🌟 [bopwiki](https://codeberg.org/Valenoern/bopwiki) – simple "microwiki" implementation, fairly similar to the Zettelkasten system

🌟 [Wolfgame](https://gitlab.com/stemid/wolfgame) – a game akin to Mafia; once started, it simulates a day/night cycle and allows players to vote on who might be a werewolf during the day

🌟 [Dharma](https://github.com/cjslep/dharma) – federated community-building platform for Eve Online corporations

🌟 [lectrn](https://github.com/lectrn/lectrn) – social network for humans that is free, decentralized, open, and easy to use

🌟 [Catcast-D](https://github.com/mrcatman/catcast-d) – federated video live streaming platform

🌟 [FChannel](https://github.com/FChannel0/FChannel-Server) – libre, self-hostable imageboard platform that utilizes ActivityPub

🌟 [MatticNote](https://github.com/MatticNote/MatticNote) – ActivityPub compatible social network that aims to be easy for everyone to use

🌟 [hvxahv](https://github.com/disism/hvxahv) – multifunctional decentralized social network implementation

→ For the full list of Fediverse projects in development see [Miscellaneous](https://fediverse.party/en/miscellaneous) page.

#### Project forks:
🥄 [Ecko](https://github.com/magicstone-dev/ecko) – fork of Mastodon to optimize toward community, that is making it as easy as possible to contribute

🥄 [Acropolis](https://github.com/magicstone-dev/acropolis) – fork of diaspora that's making it as easy as possible to contribute

#### New extensions:
💎   [Group Actor](https://git.ondrovo.com/MightyPork/group-actor) – groups work with any software that implements Mastodon client API; has moderation, admin announcements

### Fediverse ecosystem

In July [Inexcode](https://social.inex.rocks/@inexcode) relaunched [fediverse.space](https://fediverse.space) - a beautiful project that has previously been developed by Tao Bojlén and that got a second life thanks to its codebase being open source! In August Paula [launched](https://climatejustice.social/@PaulaToThePeople/106681444006722123) a new forum for any discussions related to Fediverse - [fediverse.town](https://fediverse.town). In November [Minoru](https://functional.cafe/@minoru) open sourced Fediverse [nodes list](https://nodes.fediverse.party) to help public statistics hubs and to empower new ecosystem developments! And by the end of the year, [Fediverse Wiki](https://joinfediverse.wiki) project was launched.

![Space photo background with major Fedi ecosystem URLs listed](/en/post/fediverse-in-2021/ecosystem.jpg)
<small class="u-block u-center"><span class="u-emphasize">Credits:</span> background image by Alex Antropov, Pixabay license</small>

*Let's make even more useful, quality projects around Fedi!* And, please, may they not rely on avoidable 3rd-party dependencies, like Google Fonts, Cloudflare and other centralized trackers.

More and more <span class="u-goldenBg">official institutions</span> are moving towards FOSS and Fediverse:

•  In 2021 [Noyb.eu](https://mastodon.social/@noybeu) officially established its presence (for the second time:);

•  Museums explore federated networks: [Beeld en Geluid](https://peertube.beeldengeluid.nl/accounts/nisv), the Netherlands Sound & Vision museum, and [museum of retro Computers in Kishinev](https://oldpcmuseum.ru/videos/local) installed their own PeerTube servers; European [Institute for Contemporary Art and Science](https://mastodon.art/@EICAS) opened a Mastodon account;

•  New accounts of German [politicians](https://bonn.social/@gerald_leppert/107513082220761369) keep popping up;

•  Universities are [joining](https://bonn.social/@gerald_leppert/107005341603648305) federated networks too.

There have also been ideas to set up an official Fediverse foundation, a legal entity to provide extra opportunities for contributors who want to work on improving Fediverse. [Arnold Schrijver](https://mastodon.social/@humanetech) has been working on [fedi.foundation](https://fediverse.codeberg.page/), and [Paula](https://climatejustice.social/@PaulaToThePeople) [started a discussion](https://fediverse.town/t/fediverse-foundation-lets-get-serious/299) about first steps needed for the creation of such an organization. Would you like to be part of this initiative? Join the discussion!

### Fediverse 2021 timeline

`✔ January 7:` PeerTube version 3 [is out](https://framablog.org/2021/01/07/peertube-v3-its-a-live-a-liiiiive), complete with live streaming feature and a behind-the-scenes 💡 [short film](https://framatube.org/w/hrg4zLAhJZ371toka596nS)

`✔ January 17:` Lemmur, a mobile client for [Lemmy](https://join-lemmy.org), has its [first release](https://mastodon.social/@LemmyDev/105569027196868015)

`✔ February:` [Pleroma](https://pleroma.social) surpasses the 1.000 servers milestone

`✔ March 10:` a wave of new users [migrates](https://mastodon.ml/@drq/105924843364764170) into the Russian constellation of Fediverse

`✔  April:` [NGI Zero](https://mastodon.xyz/@NGIZero/106103511651729574) [organize](https://socialhub.activitypub.rocks/pub/ec-ngi0-liaison-webinars-and-workshop-april-2021) a series of webinars and workshops called ["ActivityPub for Administrations"](https://conf.tube/videos/watch/9bb55418-5a0d-4bcd-8d41-f2dab5d531c4)

`✔ April 2:` [Castopod](https://code.podlibre.org/podlibre/castopod), a platform for podcasters, [joins Fediverse](https://podlibre.social/@Castopod/105996916094868535)

`✔ April 18:` diaspora* [releases](https://blog.diasporafoundation.org/68-diaspora-version-0-7-15-0-released) a new minor version. Jonne Haß , one of the core team members, continues work on [a new native app for Android and iOS](https://github.com/jhass/insporation) – you can already help with translations and beta test it (the app will work for pods running the develop branch v0.7.99.0 where the API is available)

`✔ May:` Fediverse holds its very own [Fedivision](https://octodon.social/@TQ/106290491235663905) song contest

`✔ May 17:` Tusky, a popular Fediverse mobile app, is [temporarily removed](https://chaos.social/@ConnyDuck/105904002285019275) from Google Play Store, because Google doesn't understand decentralization. Users who'd downloaded it on FDroid weren't affected. Google had already threatened to remove Fediverse apps [in 2020](https://mastodon.social/@Gargron/104763960269049818), so this is becoming a nice yearly tradition.

`✔ May 18:` Fediverse 🐣 celebrates its [13 birthday](https://fediverse.party/en/post/fediverse-13-2021-party-time)

`✔ May 26:` [Smithereen](https://github.com/grishka/Smithereen), a project offering the look and features similar to Vkontakte, makes its first [beta release](https://mastodon.social/@grishka/106300451824058197)

`✔ June:` [Owncast](https://owncast.online) is [awarded](https://gabekangas.com/blog/2021/06/an-exciting-update-about-owncast/) a grant by the NLnet Foundation to support work around federation and ActivityPub

`✔ June:` PeerTube [surpasses](https://mastodon.xyz/@lightone/106427846161851475) the 1.000 servers milestone and becomes second most popular ActivityPub project by node count (after Mastodon)

`✔ July 5:` [WriteFreely](https://writefreely.org), a blogging Fediverse platform, shares an up-to-date [roadmap](https://musing.studio/roadmap) for the next 6 months

`✔ July 18:` GNU Social maintainers release a long-awaited [version 2.0](https://notabug.org/diogo/gnu-social/releases) with ActivityPub support, and later publish some [updates and plans](https://www.gnusocial.rocks/v3/updates-interface-and-accessibility.html) for version 3

`✔ August 4:` [Bookwyrm](https://github.com/mouse-reeve/bookwyrm), a social reading and reviewing platform, [gets](https://tech.lgbt/@bookwyrm/106698707975867854) an [official website](https://joinbookwyrm.com)

`✔ August 8:` Pleroma 2.4.0 is [released](https://pleroma.social/announcements/2021/08/08/pleroma-major-release-2-4-0), featuring many fixes, additions and improvements

`✔ August 13:` Mastodon is registered as a [non-profit organization](https://blog.joinmastodon.org/2021/08/mastodon-now-a-non-profit-organisation)

`✔ September:` Fedi stats hub Poduptime gets re-branded and becomes [Fediverse.observer](https://fediverse.observer)

`✔ September 1:` first [Owncast](https://github.com/owncast/owncast) tests of federation over ActivityPub

`✔ September:` Facebook is [caught](https://mastodon.xyz/@lightone/106899005772803939) following Twitter's practice and [flagging](https://octodon.social/@yhancik/106897948169079191) posts with links to certain Fediverse related websites

`✔ September 8:` Pixelfed's latest [stable release](https://github.com/pixelfed/pixelfed/releases/tag/v0.11.1) adds media licenses, federated stories and improved mod and admin tools

`✔ September 25:` Friendica ["Siberian Iris"](https://github.com/friendica/friendica/releases/tag/2021.09) is out, with scheduled postings, better notification system and admin panel improvements

`✔ September 28:` [Gitea](https://gitea.io), a community-maintained Git project, adds [NodeInfo support](https://social.gitea.io/@gitea/107006650861897944), getting one step closer to ActivityPub federation between code forges

`✔ September 28:` [Funkwhale's](https://funkwhale.audio) official Android app [becomes available](https://f-droid.org/packages/audio.funkwhale.ffa/) on FDroid.

`✔ November:` Mastodon fork Truth.Social, [associated](https://blog.joinmastodon.org/2021/10/trumps-new-social-media-platform-found-using-mastodon-code/) with the former president of the United States, Donald J. Trump, [is open sourced](https://floss.social/@josias/107334378568468729) after [pressure](https://mastodon.social/@Gargron/107185524424634325) from the maintainers of Mastodon

`✔ November 9:` Hubzilla [introduces](https://framagit.org/hubzilla/core/-/tags/6.4) version 6.4 that improves file upload performance and all-in-one channel cloning via network

`✔ November 13:` GoToSocial project makes its first beta [release](https://github.com/superseriousbusiness/gotosocial/releases/tag/v0.1.0)

`✔ November 13:` Lemmur ([Lemmy](https://join-lemmy.org) client) [receives](https://lemmy.ml/post/89061) funding from [NGIZero](https://mastodon.xyz/@NGIZero) NLnet foundation

`✔ November 14:` Lemmy [starts federating](https://lemmy.ml/post/89740) with several other Fedi projects

`✔ November 21:` diaspora* core team reveales its [short-term plans](https://blog.diasporafoundation.org/70-a-big-hey-from-diaspora) for the future

`✔ November 22:` FedeProxy, a project that will allow code forges to interoperate, gets [rebranded](https://mastodon.online/@dachary/107319992161222471) to [ForgeFriends](https://forgefriends.org). Earlier in the year developers behind the project [got a grant](https://social.logilab.org/@arthurlutz/105985041778661705) from NGI DAPSI

`✔ November 23:` [Mobilizon](https://joinmobilizon.org/en) releases [version 2](https://framablog.org/2021/11/23/mobilizon-v2-now-matured-like-a-good-french-wine) with many tweaks and improvements

`✔ November 30:` PeerTube version 4 [is out](https://framablog.org/2021/11/30/peertube-v4-more-power-to-help-you-present-your-videos), bringing advanced filter features that improve moderation and administration

`✔ December 6:` [Bonfire](https://github.com/bonfire-networks) team who are working on customizable ActivityPub software share their new beautiful [website](https://bonfirenetworks.org) communicating the vision and the motivations behind the project

`✔ December 7:` [Inventaire](https://github.com/inventaire/inventaire) [becomes](https://mamot.fr/@inventaire/107405333892235843) part of Fediverse

`✔ December 26:` [Bonfire](https://github.com/bonfire-networks) is [awarded](https://bonfirenetworks.org/posts/bonfire_cos/) a grant from the Culture of Solidarity Fund to support cross-border cultural initiatives of solidarity in times of uncertainty and "infodemic"


*Is any important 2021 event missing? Feel free to suggest it in [issues](https://codeberg.org/fediverse/fediparty/issues) or send a suggestion in a [direct message](https://mastodon.xyz/@lightone)*.

### Fediverse.party news

Feneas, the organization hosting this website and its repository, may soon [shut down](https://codeberg.org/fediverse/fediparty/issues/46). This is sad news. Huge thanks to all the Feneas team, and personally to Jason Robinson and Lukas Matt, for your work, for always being patient and helpful. Good luck in all your future endeavours!

*Fediverse.party* is in search of a new home. It would be nice to move to another GitLab instance. But, as stability and reliability are more important than certain software, we'll most likely migrate to Codeberg. Let's wait and see how it works out.

<span class="u-goldenBg">Move slow and build things to last! Here's to another great year.</span> 🎉

*Fediverse is mostly run by volunteers who spend their own money to keep the network going. If you enjoy being on Fediverse, please, consider sponsoring your own local server or donating to Fedi project you most often use.*
